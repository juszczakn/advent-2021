(ns advent-2021.day2
  (:require
   [clojure.string :as s]
   [clojure.test :refer :all]))


(defn- to-directions
  [line]
  (let [[dir mag] (s/split (s/trim line) #" ")]
    {:direction (keyword dir) :magnitude (Integer. mag)}))

(defn- get-input []
  (let [txt (slurp (str (System/getProperty "user.home")
                        "/Downloads/input2.txt"))]
    (map to-directions (s/split txt #"\n"))))


;; -----------------


(defn- move1
  [location direction]
  (let [dir (:direction direction)
        mag (:magnitude direction)]
    (cond
      (= :forward dir) (update-in location [:x] + mag)
      (= :down dir) (update-in location [:y] + mag)
      (= :up dir) (update-in location [:y] - mag))))


(defn solve1
  [directions]
  (loop [rst (rest directions)
         direction (first directions)
         location {:x 0 :y 0}]
    (if (empty? rst)
      (apply * (vals (move1 location direction)))
      (recur (rest rst)
             (first rst)
             (move1 location direction)))))


(deftest solve1-test
  (testing "solve1"
    (is (= 27 (solve1 [{:direction :forward :magnitude 2}
                       {:direction :down :magnitude 4}
                       {:direction :up :magnitude 1}
                       {:direction :forward :magnitude 7}])))))


;; -----------------


(defn- move2
  [location direction]
  (let [dir (:direction direction)
        mag (:magnitude direction)
        aim (:aim location)]
    (cond
      (= :forward dir) (-> location
                           (update-in [:x] + mag)
                           (update-in [:y] + (* mag aim)))
      (= :down dir) (update-in location [:aim] + mag)
      (= :up dir) (update-in location [:aim] - mag))))


(defn solve2
  [directions]
  (loop [rst (rest directions)
         direction (first directions)
         location {:x 0 :y 0 :aim 0}]
    (if (empty? rst)
      (apply * (vals (select-keys (move2 location direction) [:x :y])))
      ;;location
      (recur (rest rst)
             (first rst)
             (move2 location direction)))))


(deftest solve2-test
  (testing "move2"
    (is (= {:x 2 :y 0 :aim 0}
           (move2 {:x 0 :y 0 :aim 0} {:direction :forward :magnitude 2})))
    (is (= {:x 2 :y 0 :aim 5}
           (move2 {:x 2 :y 0 :aim 0} {:direction :down :magnitude 5}))))
  (testing "solve2"
    (is (= 900 (solve2 [{:direction :forward :magnitude 5}
                        {:direction :down :magnitude 5}
                        {:direction :forward :magnitude 8}
                        {:direction :up :magnitude 3}
                        {:direction :down :magnitude 8}
                        {:direction :forward :magnitude 2}])))))

