(ns advent-2021.day4
  (:require
   [clojure.string :as str]
   [clojure.test :refer :all]
   [clojure.spec.alpha :as s]
   [orchestra.spec.test :as st]))

(def +test-example-input+
  "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7")

(defn- get-lines []
  (let [txt (slurp (str (System/getProperty "user.home")
                        "/Downloads/input4.txt"))]
    txt))


(defn- to-board [board-lines]
  (let [rows-w-nums (map #(str/split (str/trim %) #" +") board-lines)
        rows (map (fn [row] (map #(Integer. %) row)) rows-w-nums)
        cols-flat (for [idx [0 1 2 3 4]
                        row rows]
                    (nth row idx))
        rows-flat (flatten rows)]
    (partition 5 (concat rows-flat cols-flat))))


(s/def ::guesses (s/coll-of int?))
(s/def ::board (s/coll-of (s/coll-of int?)))
(s/def ::boards (s/coll-of ::board))
(s/def ::input (s/keys :req-un [::guesses ::boards]))

(s/fdef get-input
  :args (s/cat :input string?)
  :ret ::input)

(defn- get-input [txt]
  (let [lines (str/split txt #"\n")
        guesses (map #(Integer. (str/trim %))
                     (str/split (first lines) #","))
        boards-lines (partition 5 (filter #(not (= "" %)) (rest lines)))
        boards (map to-board boards-lines)]
    {:guesses guesses :boards boards}))


;; -------------


(s/def ::board-winners (s/coll-of (s/coll-of int?)))

(s/fdef won?
  :args (s/cat :board-winners ::board-winners
               :curr-guesses ::guesses)
  :ret boolean?)

(defn- won? [board-winners curr-guesses]
  (let [curr-guesses (set curr-guesses)
        check (fn [winner] (count (clojure.set/intersection curr-guesses winner)))
        matches (map check board-winners)]
    (not (empty? (filter #(= 5 %) matches)))))

(deftest won-test
  (st/instrument)
  (let [won-ex '(#{24 4 21 17 14}
                 #{15 19 9 16 10}
                 #{20 23 26 18 8}
                 #{13 22 6 11 5}
                 #{0 7 3 12 2}
                 #{22 2 14 10 18}
                 #{0 21 11 16 8}
                 #{15 13 17 12 23}
                 #{24 6 3 9 26}
                 #{7 20 4 19 5})]
    (testing "won?"
      (is (= false (won? won-ex [1 2 3 4 5])))
      (is (= false (won? won-ex [24 6 3 9])))
      (is (= true (won? won-ex [24 6 3 9 26]))))))


(s/fdef calc-score
  :args (s/cat :board ::board-winners
               :guesses ::guesses)
  :ret int?)

(defn- calc-score [board guesses]
  (let [all (reduce #(into % %2) #{} board)
        un-guessed (clojure.set/difference all (set guesses))]
    (* (last guesses) (reduce + un-guessed))))


(s/fdef solve-boards
  :args (s/cat :guesses ::guesses
               :boards ::boards)
  :ret (s/or :score int? :nil nil?))

(defn- solve-boards
  [guesses boards]
  (loop [rst (rest boards)
         board (first boards)]
    (if (won? board guesses)
      (calc-score board guesses)
      (if (empty? rst)
        nil
        (recur (rest rst) (first rst))))))

(s/fdef solve1
  :args (s/cat :arg1 ::input)
  :ret int?)

(defn solve1
  [{guesses :guesses boards :boards}]
  (let [winners (map #(map set %) boards)]
    (loop [rst-guesses (drop 5 guesses)
           curr-guesses (take 5 guesses)]
      (let [solved (solve-boards curr-guesses winners)]
        (if solved
          solved
          (recur (rest rst-guesses)
                 (concat curr-guesses [(first rst-guesses)])))))))


(deftest solve1-test
  (st/instrument)
  (let [input (get-input +test-example-input+)
        won-ex '(#{24 4 21 17 14}
                 #{15 19 9 16 10}
                 #{20 23 26 18 8}
                 #{13 22 6 11 5}
                 #{0 7 3 12 2}
                 #{22 2 14 10 18}
                 #{0 21 11 16 8}
                 #{15 13 17 12 23}
                 #{24 6 3 9 26}
                 #{7 20 4 19 5})]
    (testing "won?"
      (is (= false (won? won-ex [1 2 3 4 5])))
      (is (= false (won? won-ex [24 6 3 9])))
      (is (= true (won? won-ex [24 6 3 9 26]))))
    (testing "solve1"
      (is (= 4512 (solve1 input))))))


;; -------------


(s/fdef unsolved-boards
  :args (s/cat :guesses ::guesses :boards ::boards ::prev-guess int?)
  :ret (s/or :nil nil? :score int?))

(defn- unsolved-boards
  [guesses boards prev-guess]
  (loop [rst (rest boards)
         board (first boards)]
    (if (not (won? board guesses))
      (calc-score board (concat guesses [prev-guess]))
      (if (empty? rst)
        nil
        (recur (rest rst) (first rst))))))

(s/fdef solve2
  :args (s/cat :arg1 ::input)
  :ret int?)

(defn solve2
  [{guesses :guesses boards :boards}]
  (let [winners (map #(map set %) boards)]
    (loop [curr-guesses (reverse guesses)
           prev-guess nil]
      (let [unsolved (unsolved-boards curr-guesses winners prev-guess)]
        (if unsolved
          unsolved
          (recur (rest curr-guesses)
                 (first curr-guesses)))))))


(deftest solve2-test
  (st/instrument)
  (let [input (get-input +test-example-input+)]
    (testing "solve2"
      (is (= 1924 (solve2 input))))))
