(ns advent-2021.core
  (:gen-class)
  (:require
   [clojure.string :as str]))

(defn -main
  [& args]
  (println
   "I don't do anything. See the day#.clj files,
or load them in the repl with something like:
  (load \"day1\")
  (in-ns 'advent-2021.day1)"))


(defn- get-input-file-path
  [file-name]
  (str (System/getProperty "user.home")
       "/Downloads/" file-name))

(defn read-input-file [file-name]
  (str/trim (slurp (get-input-file-path file-name))))
