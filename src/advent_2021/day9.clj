(ns advent-2021.day9
  (:require
   [advent-2021.core :as core]
   [clojure.string :as str]
   [clojure.set :refer [union difference]]
   [clojure.test :refer :all]
   [clojure.spec.alpha :as s]
   [orchestra.spec.test :as st]))


(def ^:private +test-input+
  "2199943210
3987894921
9856789892
8767896789
9899965678")


(defn- parse
  [txt]
  (let [lines (str/split txt #"\n")
        ints (mapv #(str/split % #"") lines)]
    (mapv (fn [line] (mapv #(Integer. %) line)) ints)))


(defn get-input
  ([] (get-input (core/read-input-file "input9.txt")))
  ([txt] (parse txt)))


;; -------------


(defn- get-xy
  [m x y]
  (if (or (>= -1 x) (>= -1 y)
          (>= x (count (first m)))
          (>= y (count m)))
    10
    (-> m (nth y) (nth x))))

(defn- surroundings
  "get x,y; then all ints around it."
  [m x y]
  (let [get-xy (partial get-xy m)
        p (get-xy x y)
        x-1 (get-xy (- x 1) y)
        y-1 (get-xy x (- y 1))
        x+1 (get-xy (+ x 1) y)
        y+1 (get-xy x (+ 1 y))]
    [p x-1 y-1 x+1 y+1]))


(defn- combos
  [input]
  (for [y (range (count input))
        x (range (count (first input)))]
    (surroundings input x y)))


(defn solve1
  [input]
  (let [cs (combos input)]
    (->> cs
         (filter #(< (first %) (apply min (rest %))))
         (map #(+ 1 (first %)))
         (apply +))))


;; -------------


(defn- fill
  "fill algo starting at x,y."
  [m x y]
  (let [to-check (list {:x x :y y :v (get-xy m x y)})]
    (loop [to-check to-check
           area #{}]
      (if (empty? to-check)
        area
        (let [{:keys [x y v] :as p} (first to-check)
              valley? (and (< v 9) (not (area p)))
              next-checks (if valley?
                            (let [[x-1 y-1 x+1 y+1] (rest (surroundings m x y))]
                              (list {:x (- x 1) :y y :v x-1}
                                    {:x (+ x 1) :y y :v x+1}
                                    {:x x :y (- y 1) :v y-1}
                                    {:x x :y (+ y 1) :v y+1}))
                            '())]
          (recur (concat next-checks (rest to-check))
                 (if valley? (conj area p) area)))))))


(defn solve2
  [input]
  (let [points (for [y (range (count input))
                     x (range (count (first input)))]
                 [x y])]
    (->> points
         (reduce (fn [agg [x y]] (conj agg (fill input x y)))
                 #{})
         (map count)
         sort
         reverse
         (take 3)
         (apply *))))
