(ns advent-2021.day7
  (:require
   [advent-2021.core :as core]
   [clojure.string :as str]
   [clojure.test :refer :all]
   [clojure.spec.alpha :as s]
   [orchestra.spec.test :as st]))


(def ^:private +test-example-input+
  "16,1,2,0,4,2,7,1,2,14")


(defn get-input
  ([] (get-input (str/trim (core/read-input-file "input7.txt"))))
  ([txt]
   (map #(Integer. %) (str/split txt #","))))

;; -------------


(defn- fuel-count
  [crabs x]
  (apply + (map #(Math/abs (- % x)) crabs)))

(defn solve1
  [input]
  (let [mn (apply min input)
        mx (apply max input)]
    (loop [x mn
           lowest Integer/MAX_VALUE]
      (if (= x mx)
        lowest
        (recur (+ 1 x)
               (min lowest (fuel-count input x)))))))

(deftest solve1-test
  (st/instrument)
  (testing "solve1"
    (is (= 37 (solve1 (get-input +test-example-input+))))))


;; -------------


(defn- sum-of-ints
  [x1 x2]
  (let [n (Math/abs (- x1 x2))]
    (/ (* n (+ n 1)) 2)))


(defn- fuel-count-exp
  [crabs x]
  (apply + (map #(sum-of-ints x %) crabs)))


(defn solve2
  [input]
  (let [mn (apply min input)
        mx (apply max input)]
    (loop [x mn
           lowest Integer/MAX_VALUE]
      (if (= x mx)
        lowest
        (recur (+ 1 x)
               (min lowest (fuel-count-exp input x)))))))


(deftest solve2-test
  (testing "solve2"
    (is (= 168 (solve2 (get-input +test-example-input+))))))
