(ns advent-2021.day8
  (:require
   [advent-2021.core :as core]
   [clojure.string :as str]
   [clojure.set :refer [union difference]]
   [clojure.test :refer :all]
   [clojure.spec.alpha :as s]
   [orchestra.spec.test :as st]))


(def ^:private +test-input+
  "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce")


(defn- parse
  [txt]
  (let [lines (str/split txt #"\n")
        things (map #(str/split % #" ") lines)
        maps #(set (map % %2))
        to-keywords (fn [s] (maps (fn [c] (keyword (str c))) s))]
    (mapv #(hash-map :in (mapv to-keywords (take 10 %))
                     :out (mapv to-keywords (drop 11 %))) things)))

(s/def ::seg #{:a :b :c :d :e :f :g})
(s/def ::sev-seg (s/coll-of ::seg))
(s/def ::sev-segs (s/coll-of ::sev-seg))
(s/def ::in ::sev-segs)
(s/def ::out ::sev-segs)
(s/def ::row (s/keys :req-un [::in ::out]))
(s/def ::input (s/coll-of ::row))

(s/fdef get-input
  :args (s/cat :txt string?)
  :ret ::input)

(defn get-input
  ([] (get-input (str/trim (core/read-input-file "input8.txt"))))
  ([txt] (parse txt)))


;; -------------


(s/fdef solve1
  :args ::input
  :ret integer?)

(defn solve1
  [input]
  (->> input
       (map :out)
       (apply concat)
       (map count)
       (filter #{2 4 3 7}) ;; 1 4 7 8
       count))

;; -------------

(s/def ::combos
  (s/map-of integer? ::sev-seg))

(s/fdef known-combos
  :args (s/cat :sev-segs ::sev-segs)
  :ret ::combos)

(defn- known-combos
  [sev-segs]
  (let [unq {2 1 4 4 3 7 7 8}
        m (group-by count sev-segs)
        known (reduce (fn [m [x [y]]] (conj m [(unq x) y])) {} m)]
    (select-keys known (vals unq))))


(defn- ident
  "Identify in `in`, the next unique num based on a `cmp` comparable
  known number and the expected number of `diff`erent segments between
  the two."
  [in known cmp diff]
  (let [known (set known)
        f (fn [ks]
            (and (not (known ks))
                 (cond
                   (zero? diff) (= ks cmp)
                   (< 0 diff) (= diff (count (difference ks cmp)))
                   :else (= diff (- (count (difference cmp ks)))))))]
    (first (filter f in))))


(defn- combos
  "Reason out all of the different number combinations, return a
  mapping."
  [input]
  (let [in (:in input)
        m (known-combos in)
        ;; 1 4 7 8
        three (ident in (vals m) (m 1) 3)
        m (merge m {3 three})
        nine (ident in (vals m) (union (m 4) three) 0)
        m (merge m {9 nine})
        five (ident in (vals m) (difference nine (m 1)) 1)
        m (merge m {5 five})
        six (ident in (vals m) five 1)
        m (merge m {6 six})
        zero (ident in (vals m) (m 8) -1)
        m (merge m {0 zero})
        two (ident in (vals m) (m 8) -2)
        m (merge m {2 two})]
    m))


(defn- decode
  "Using the combo mapping and the input to figure out what the output is."
  [m input]
  (let [m (clojure.set/map-invert m)
        out (map m (:out input))]
    (Integer. (str/join out))))


(s/fdef solve2
  :args ::input
  :ret integer?)

(defn solve2
  [input]
  (apply + (map #(decode (combos %) %) input)))
