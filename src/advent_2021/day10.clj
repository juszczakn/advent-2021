(ns advent-2021.day10
  (:require
   [advent-2021.core :as core]
   [clojure.string :as str]
   [clojure.test :refer :all]
   [clojure.spec.alpha :as s]
   [orchestra.spec.test :as st]))


(def ^:private test-input
  "[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]")


(defn- parse
  [txt]
  (let [lines (str/split txt #"\n")]
    (mapv #(str/split % #"") lines)))


(defn get-input
  ([] (get-input (core/read-input-file "input10.txt")))
  ([txt] (parse txt)))


;; -------------

(def ^:private open-close
  {"(" ")"
   "{" "}"
   "[" "]"
   "<" ">"})

(def ^:private close-open
  (clojure.set/map-invert open-close))

(def ^:private scoring
  {")" 3
   "]" 57
   "}" 1197
   ">" 25137})


(defn- first-invalid
  [syms]
  (loop [sym (first syms)
         syms syms
         stack '()]
    (if (empty? syms)
      nil
      (let [close? (close-open sym)]
        (cond
          (and close? (not= close? (first stack))) sym
          close? (recur (second syms) (rest syms) (rest stack))
          :else (recur (second syms) (rest syms) (conj stack sym)))))))



(defn solve1
  [input]
  (->> input
       (map first-invalid)
       (filter #(not (nil? %)))
       (map scoring)
       (apply +)))


;; -------------


(defn- remaining-to-close
  [syms]
  (loop [sym (first syms)
         syms syms
         stack '()]
    (if (empty? syms)
      stack
      (let [close? (close-open sym)]
        (cond
          close? (recur (second syms) (rest syms) (rest stack))
          :else (recur (second syms) (rest syms) (conj stack sym)))))))


(def ^:private close-scoring
  {"(" 1
   "[" 2
   "{" 3
   "<" 4})


(defn- close-score
  [stack]
  (->> stack
       (reduce #(+ (close-scoring %2) (* % 5)) 0)))


(defn solve2
  [input]
  (let [xs (->> input
                (filter (complement first-invalid))
                (map remaining-to-close)
                (map close-score)
                sort)
        middle-idx (/ (- (count xs) 1) 2)]
    (nth xs middle-idx)))
