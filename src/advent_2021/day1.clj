(ns advent-2021.day1
  (:require
   [clojure.string :as s]))

(def +input1+
  (let [txt (slurp (str (System/getProperty "user.home")
                        "/Downloads/input.txt"))]
    (map #(Integer. %) (s/split txt #"\n"))))

;; -------------

(defn solve1 [all-depths]
  "The schemey way."
  (loop [depths (rest all-depths)
         last (Integer/MAX_VALUE)
         next (first all-depths)
         counter 0]
    (if (empty? depths)
      (+ counter (if (< last next) 1 0))
      (recur (rest depths)
             next
             (first depths)
             (+ counter (if (< last next) 1 0))))))

;; -------------

(defn solve2 [all-depths]
  "The same, but different."
  (loop [depths (rest all-depths)
         size (count all-depths)
         one (reduce + (take 3 all-depths))
         two (reduce + (take 3 (drop 1 all-depths)))
         counter 0]
    (if (<= size 4)
      (+ counter (if (> two one) 1 0))
      (recur (rest depths)
             (dec size)
             two
             (reduce + (take 3 (drop 1 depths)))
             (+ counter (if (> two one) 1 0))))))
