(ns advent-2021.day6
  (:require
   [advent-2021.core :as core]
   [clojure.string :as str]
   [clojure.test :refer :all]
   [clojure.spec.alpha :as s]
   [orchestra.spec.test :as st]))


(def ^:private +test-example-input+
  "3,4,3,1,2")

(s/def ::fish (s/coll-of int?))

(s/fdef get-input
  :args (s/alt
         :nullary (s/cat)
         :unary (s/cat :txt string?))
  :ret ::fish)

(defn get-input
  ([] (get-input (str/trim (core/read-input-file "input6.txt"))))
  ([txt]
   (map #(Integer. %) (str/split txt #","))))

;; -------------


(defn- calc-fish
  "Decrease fish age by 1, calculate new day 6 and 8 fish."
  [fish-by-age]
  (let [txs (mapv vector (range -1 8) (range 0 9))
        swap-fish-counts (fn [m v] (update m (first v) (fn [_] (m (second v) 0))))
        new-fish-by-age (reduce swap-fish-counts fish-by-age txs)
        cnt (new-fish-by-age -1)]
    (-> new-fish-by-age
     (update 6 #(+ % cnt))
     (update 8 (fn [_] cnt)))))


(deftest calc-fish-test
  (testing "calc-fish"
    (is (= {-1 5 0 1 1 2 2 3 3 4 4 5 5 6 6 12 7 8 8 5}
           (calc-fish {-1 10 0 5 1 1 2 2 3 3 4 4 5 5 6 6 7 7 8 8})))))

(defn- solve
  [input days]
  (let [fishies (group-by identity input)
        fish-by-age (reduce #(update-in % [%2] count) fishies (keys fishies))]
    (loop [fish fish-by-age
           n 0]
      (if (= n days)
        (apply + (vals (dissoc fish -1)))
        (recur (calc-fish fish)
               (+ 1 n))))))


(s/fdef solve1
  :args (s/cat :input ::fish)
  :ret int?)

(defn solve1
  [input]
  (solve input 80))

(s/fdef solve2
  :args (s/cat :input ::fish)
  :ret int?)

(defn solve2
  [input]
  (solve input 256))

(deftest solve1-test
  (st/instrument)
  (let [test-input (get-input +test-example-input+)]
    (testing "solve1"
      (is (= 5934 (solve1 test-input))))
    (testing "solve2"
      (is (= 26984457539 (solve2 test-input))))))
