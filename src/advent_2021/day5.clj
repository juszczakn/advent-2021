(ns advent-2021.day5
  (:require
   [clojure.string :as str]
   [clojure.test :refer :all]
   [clojure.spec.alpha :as s]
   [orchestra.spec.test :as st]))

(def +test-example-input+
  "0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2")


(def input-file-path
  (str (System/getProperty "user.home")
       "/Downloads/input5.txt"))

(defn- read-input-file []
  (slurp input-file-path))


(s/def ::x1 int?)
(s/def ::x2 int?)
(s/def ::y1 int?)
(s/def ::y2 int?)
(s/def ::points (s/keys :req-un [::x1 ::x2 ::y1 ::y2]))


(s/fdef to-points
  :args (s/cat :strs (s/coll-of string?))
  :ret ::points)

(defn- to-points [strs]
  (let [[p1 p2] (map #(str/split % #",") strs)
        [x1 y1] p1
        [x2 y2] p2]
    {:x1 (Integer. x1) :x2 (Integer. x2)
     :y1 (Integer. y1) :y2 (Integer. y2)}))

(s/fdef get-input
  :args (s/alt
         :nullary (s/cat)
         :unary (s/cat :txt string?))
  :ret (s/coll-of ::points))

(defn get-input
  ([]
   (get-input (read-input-file)))
  ([txt]
   (let [lines (str/split txt #"\n")
         points-txt (map #(str/split % #" -> ") lines)
         points (map to-points points-txt)]
     points)))

(deftest get-input-test
  (st/instrument)
  (testing "get-input"
    (is (= (get-input +test-example-input+)
           '({:x1 0, :x2 5, :y1 9, :y2 9}
             {:x1 8, :x2 0, :y1 0, :y2 8}
             {:x1 9, :x2 3, :y1 4, :y2 4}
             {:x1 2, :x2 2, :y1 2, :y2 1}
             {:x1 7, :x2 7, :y1 0, :y2 4}
             {:x1 6, :x2 2, :y1 4, :y2 0}
             {:x1 0, :x2 2, :y1 9, :y2 9}
             {:x1 3, :x2 1, :y1 4, :y2 4}
             {:x1 0, :x2 8, :y1 0, :y2 8}
             {:x1 5, :x2 8, :y1 5, :y2 2})))))


;; -------------


(defn- get-change [p1 p2]
  (cond
    (= p1 p2) 0
    (< p1 p2) 1
    (> p1 p2) -1))

(s/def ::field (s/coll-of (s/coll-of int?)))

(s/fdef process-field
  :args (s/cat :field ::field
               :line ::points)
  :ret ::field)

(defn- process-field
  [field line]
  ;; `*-in` doesn't work on lists because they're not "associative"?
  (let [field (into [] field)
        {:keys [x1 x2 y1 y2]} line
        dx (get-change x1 x2)
        dy (get-change y1 y2)]
    (loop [field field
           x x1
           y y1]
      (if (and (= x x2) (= y y2))
        (update-in field [y x] + 1)
        (recur (update-in field [y x] + 1)
               (+ x dx)
               (+ y dy))))))

(deftest process-field-test
  (st/instrument)
  (testing "process-field"
    (is (= [[2 2 3] [5 5 6] [8 8 9]]
           (process-field '([1 2 3] [4 5 6] [7 8 9])
                          {:x1 0 :x2 0 :y1 0 :y2 2})))
    (is (= [[1 2 3] [4 6 7] [7 8 9]]
           (process-field [[1 2 3] [4 5 6] [7 8 9]]
                          {:x1 1 :x2 2 :y1 1 :y2 1})))
    (is (= [[1 2 3] [4 5 6] [8 9 10]]
           (process-field [[1 2 3] [4 5 6] [7 8 9]]
                          {:x1 0 :x2 2 :y1 2 :y2 2})))
    (is (= [[2 2 3] [4 6 6] [7 8 10]]
           (process-field [[1 2 3] [4 5 6] [7 8 9]]
                          {:x1 0 :x2 2 :y1 0 :y2 2})))))

(defn- count-field [field cutoff]
  (count (filter #(>= % cutoff) (flatten field))))

(defn- init-empty-field
  [input]
  (let [xs (flatten (map #(vector (:x1 %) (:x2 %)) input))
        ys (flatten (map #(vector (:y1 %) (:y2 %)) input))
        max-x (apply max xs)
        max-y (apply max ys)
        empty-field (repeat (+ 1 max-y) (vec (repeat (+ 1 max-x) 0)))]
    (into [] empty-field)))


(s/fdef solve1
  :args (s/cat :input (s/coll-of ::points))
  :ret int?)

(defn solve1
  [input]
  (let [hor-vert-lines (filter #(or (= (:x1 %) (:x2 %)) (= (:y1 %) (:y2 %))) input)
        empty-field (init-empty-field input)]
    (loop [lines hor-vert-lines
           line (first hor-vert-lines)
           field empty-field]
      (if (empty? lines)
        (count-field field 2)
        (recur (rest lines)
               (second lines)
               (process-field field line))))))


(deftest solve1-test
  (st/instrument)
  (testing "solve1"
    (is (= 5 (solve1 (get-input +test-example-input+))))))


;; -------------


(s/fdef solve2
  :args (s/cat :input (s/coll-of ::points))
  :ret int?)

(defn solve2
  [input]
  (let [empty-field (init-empty-field input)]
    (loop [lines input
           line (first input)
           field empty-field]
      (if (empty? lines)
        (count-field field 2)
        (recur (rest lines)
               (second lines)
               (process-field field line))))))

(deftest solve2-test
  (st/instrument)
  (testing "solve2"
    (is (= 12 (solve2 (get-input +test-example-input+))))))
