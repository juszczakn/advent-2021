(ns advent-2021.day3
  (:require
   [clojure.string :as s]
   [clojure.test :refer :all]))

(defn- to-binary-lst [row]
  (map #(Integer. %) (s/split row #"")))

(defn- get-input []
  (let [txt (slurp (str (System/getProperty "user.home")
                        "/Downloads/input3.txt"))]
    (map to-binary-lst (s/split txt #"\n"))))

;; -------------

(defn- powers [start exp]
  "(powers 1 2) => '(1 2 4 8 16 32 ...)"
  (lazy-cat
   [start]
   (powers (* start exp) exp)))


(defn- binary->decimal [binary]
  (let [mult (reverse (take (count binary) (powers 1 2)))]
    (apply + (map * binary mult))))


(defn solve1
  [input]
  (let [size (count input)
        sums (apply (partial map +) input)
        gamma (map #(if (<= (/ size 2) %) 1 0) sums)
        epsilon (map #(if (> (/ size 2) %) 1 0) sums)]
    (* (binary->decimal gamma)
       (binary->decimal epsilon))))


(deftest solve1-test
  (testing "binary->decimal"
    (is (= 22 (binary->decimal [1 0 1 1 0]))))
  (testing "solve1"
    (is (= 198 (solve1 [[0 0 1 0 0]
                        [1 1 1 1 0]
                        [1 0 1 1 0]
                        [1 0 1 1 1]
                        [1 0 1 0 1]
                        [0 1 1 1 1]
                        [0 0 1 1 1]
                        [1 1 1 0 0]
                        [1 0 0 0 0]
                        [1 1 0 0 1]
                        [0 0 0 1 0]
                        [0 1 0 1 0]])))))

;; -------------


(defn- filter-out
  [input n cmp]
  (let [size (count input)
        sums (apply (partial map +) input)
        elts (map #(if (cmp (/ size 2) %) 1 0) sums)]
    (filter #(= (nth elts n) (nth % n)) input)))


(defn- get-rating
  [input cmp]
  (loop [elts input
         n 0]
    (if (= 1 (count elts))
      (binary->decimal (first elts))
      (recur (filter-out elts n cmp)
             (+ 1 n)))))

(defn solve2
  [input]
  (let [oxy (get-rating input <=)
        co2 (get-rating input >)]
    (* oxy co2)))


(deftest solve2-test
  (testing "oxy-rating"
    (is (= 23 (get-rating
               [[0 0 1 0 0]
                [1 1 1 1 0]
                [1 0 1 1 0]
                [1 0 1 1 1]
                [1 0 1 0 1]
                [0 1 1 1 1]
                [0 0 1 1 1]
                [1 1 1 0 0]
                [1 0 0 0 0]
                [1 1 0 0 1]
                [0 0 0 1 0]
                [0 1 0 1 0]]
               <=))))
  (testing "co2-rating"
    (is (= 10 (get-rating
               [[0 0 1 0 0]
                [1 1 1 1 0]
                [1 0 1 1 0]
                [1 0 1 1 1]
                [1 0 1 0 1]
                [0 1 1 1 1]
                [0 0 1 1 1]
                [1 1 1 0 0]
                [1 0 0 0 0]
                [1 1 0 0 1]
                [0 0 0 1 0]
                [0 1 0 1 0]]
               >))))
  (testing "solve2"
    (is (= 230 (solve2 [[0 0 1 0 0]
                        [1 1 1 1 0]
                        [1 0 1 1 0]
                        [1 0 1 1 1]
                        [1 0 1 0 1]
                        [0 1 1 1 1]
                        [0 0 1 1 1]
                        [1 1 1 0 0]
                        [1 0 0 0 0]
                        [1 1 0 0 1]
                        [0 0 0 1 0]
                        [0 1 0 1 0]])))))
